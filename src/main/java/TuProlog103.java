import alice.tuprolog.Prolog;
import alice.tuprolog.SolveInfo;
import alice.tuprolog.exceptions.InvalidObjectIdException;
import alice.tuprolog.exceptions.MalformedGoalException;
import alice.tuprolog.exceptions.NoSolutionException;
import alice.tuprolog.lib.OOLibrary;

import static alice.tuprolog.Terms.atom;

public class TuProlog103 {

    static class ExampleClass {
        private long property = -1;

        public long getProperty() {
            return property;
        }

        public void setProperty(long property) {
            this.property = property;
        }
    }

    public static void main(String[] args) {

        /*
         * This is an empty engine with some default pre-loaded libraries and operators.
         * Pre-loaded libraries: Prolog standard library + IO library + OOLibrary operators
         * Pre-loaded operators: standard prolog operators + OOLibrary operators
         *
         * Its theory is currently empty.
         */
        Prolog engine = new Prolog();

        /*
         * Let's check which libraries are loaded by default
         * (libraries are identifies by the name of their classes)
         */
        for (String lib : engine.getLibraryManager().getCurrentLibraries()) {
            System.out.println(lib);
            /*
             * Expected results:
             * - alice.tuprolog.lib.BasicLibrary
             * - alice.tuprolog.lib.ISOLibrary
             * - alice.tuprolog.lib.IOLibrary
             * - alice.tuprolog.lib.OOLibrary
             */
        }

        // let's retrieve the reference to the OOLibrary
        OOLibrary oopLib = (OOLibrary) engine.getLibrary(OOLibrary.class.getName());

        // let's create a new instance of ExampleClass
        ExampleClass exampleObj = new ExampleClass();

        // let's register a new instance of ExampleClass as "exampleObj"
        try {
            oopLib.register(atom("exampleObj"), exampleObj);
        } catch (InvalidObjectIdException e) {
            // the provided id may be invalid (e.g., if it is not an atom)
            e.printStackTrace();
        }

        // let's perform some queries to the ExampleClass registered above

        SolveInfo si;

        try {
            // calling a something-returning function
            si = engine.solve("exampleObj <- getProperty returns X");
            System.out.println(si.isSuccess()); // should be true
            System.out.println(si.getVarValue("X")); // should be -1

            // calling a void function with arguments
            si = engine.solve("exampleObj <- setProperty(42)");
            System.out.println(si.isSuccess()); // should be true

            si = engine.solve("exampleObj <- getProperty returns X");
            System.out.println(si.isSuccess()); // should be true
            System.out.println(si.getVarValue("X")); // should be 42
        } catch (MalformedGoalException | NoSolutionException e) {
            e.printStackTrace();
        }

        System.out.println(exampleObj.getProperty()); // should be 42
    }
}
