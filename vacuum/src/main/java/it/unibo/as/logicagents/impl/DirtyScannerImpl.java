package it.unibo.as.logicagents.impl;

import it.unibo.as.logicagents.DirtyScanner;
import it.unibo.as.logicagents.VacuumEnvironment;

/**
 * This is a sensor artifact, detecting whether the environment position the agent is laying on is dirty or not
 */
public class DirtyScannerImpl extends AbstractArtefact<VacuumEnvironment> implements DirtyScanner {

    public DirtyScannerImpl(VacuumEnvironment environment) {
        super(environment);
    }

    public String scan() throws Exception {
        if (getEnvironment().isDirty(getEnvironment().getVacuumPosition())) {
            return "dirty";
        }

        return "clean";
    }
}
