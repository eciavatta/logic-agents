package it.unibo.as.logicagents;

import it.unibo.as.logicagents.impl.VacuumEnvironmentImpl;

import java.util.function.Consumer;

/**
 * A 2D, discrete environment for the vacuum agent.
 * It keeps track of:
 * <ul>
 *     <li>the arena size</li>
 *     <li>the vacuum {@link Vector2D} within the arena (1-based indexes)</li>
*      <li>the vacuum {@link Orientation} within the arena</li>
 *     <li>the {@link Vector2D} of the dirty cells</li>
 * </ul>
 * plus, it provides a number of handy methods for checking or manipulating the environment.
 *
 * Notice that this class is to be instantiated as an anonymous class, possibly overriding one or more of the three callbacks it exposes
 */
public interface VacuumEnvironment extends Environment {

    static VacuumEnvironment create(int width, int height, Vector2D vacuumPosition, Orientation orientation, Vector2D... dirty) {
        return new VacuumEnvironmentImpl(width, height, vacuumPosition, orientation, dirty);
    }

    static VacuumEnvironment createWithRandomRobotPose(int width, int height, Vector2D... dirty) {
        return create(width, height, Vector2D.randomIn(width, height), Orientation.random(), dirty);
    }

    int getWidth();

    int getHeight();

    void cleanUp(Vector2D position);

    default void cleanUp(int x, int y) {
        cleanUp(Vector2D.of(x, y));
    }

    void setDirty(Vector2D position);

    default void setDirty(int x, int y) {
        setDirty(Vector2D.of(x, y));
    }

    default void setRandomlyDirty() {
        setDirty(Vector2D.randomIn(getWidth(), getHeight()));
    }

    boolean isDirty(Vector2D position);

    default boolean isDirty(int x, int y) {
        return isDirty(Vector2D.of(x, y));
    }

    Vector2D getVacuumPosition();

    void setVacuumPosition(Vector2D vacuumPosition);

    default void setVacuumPosition(int x, int y) {
        setVacuumPosition(Vector2D.of(x, y));
    }

    Orientation getVacuumOrientation();

    void setVacuumOrientation(Orientation vacuumOrientation);

    default boolean isInside(int x, int y) {
        return x >= 1 && x <= getWidth()
                && y >= 1 && y <= getHeight();
    }

    default boolean isInside(Vector2D position) {
        return isInside(position.getX(), position.getY());
    }

    /**
     * Registers a callback to be called upon environment initialisation
     */
    void onInit(Consumer<VacuumEnvironment> listener);

    /**
     * Registers a callback to be called upon any environment variation
     */
    void onEnvironmentChanged(Consumer<VacuumEnvironment> listener);

    /**
     * Registers a callback to be called every time the vacuum moves
     */
    void onVacuumMoved(Consumer<VacuumEnvironment> listener);
}
