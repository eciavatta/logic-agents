package it.unibo.as.logicagents;

import alice.tuprolog.Prolog;

public interface Environment {
    Prolog getProlog();

    void initialize();
}
