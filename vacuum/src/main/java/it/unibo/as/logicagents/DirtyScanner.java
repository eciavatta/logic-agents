package it.unibo.as.logicagents;

import it.unibo.as.logicagents.impl.DirtyScannerImpl;

/**
 * This is a sensor artifact, detecting whether the environment position the agent is laying on is dirty or not
 */
public interface DirtyScanner extends Artefact<VacuumEnvironment> {

    static DirtyScanner create(VacuumEnvironment environment) {
        return new DirtyScannerImpl(environment);
    }

    /**
     * Scans the calling agent's location for dirty
     * @return either <code>"dirty"</code> or <code>"clean"</code>
     * @throws Exception never
     */
    String scan() throws Exception;
}
