start :-
    figure_out_initial_pose,
    discover_environment_size,
    clean_environment.

figure_out_initial_pose :-
    store(current_position, (0, 0)),
    store(facing, top),
    store(rotation_count, 0),
    store(turning, left).

discover_environment_size :-
    move_until_wall.

clean_environment :-
    clean_if_needed,
    step(left),
    clean_until_wall.

clean_until_wall :-
    clean_if_needed,
    step(forward), !,
    clean_until_wall.

clean_until_wall :-
    println(["Wall reached!"]),
    get(turning, T),
    step(T), !,
    clean_if_needed,
    step(T),
    opposite(T, NT),
    update(turning, NT),
    clean_until_wall.

clean_until_wall :-
    println(["Completed."]).

step(D) :-
    move(D),
    count_steps(S),
    S > 0,
    get(facing, F),
    get(current_position, (X, Y)),
    estimate_next_facing(F, D, NF),
    estimate_next_position(X, Y, NF, NX, NY),
    update(facing, NF),
    update(current_position, (NX, NY)),
    println(["Move to (", NX, ", ", NY, ")"]).

move_until_wall :-
    step(forward), !,
    move_until_wall.

move_until_wall :-
    println(["Wall reached!"]),
    get(rotation_count, R),
    Q is R + 1,
    update(rotation_count, Q),
    Q < 4, !,
    step(left),
    move_until_wall.

move_until_wall :-
    println(["Boundary completed."]).

clean_if_needed :-
    get(current_position, (X, Y)),
    scan(dirty),
    println(["It's dirty in (", X, ", ", Y, "): sucking up"]),
    clean_up.
clean_if_needed.
