
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Misc Utilities
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% ----------------------------------------------------------------------------------------------------------------------
% Estimation utils
% ----------------------------------------------------------------------------------------------------------------------

% estimate_next_facing(+atom, +atom, -atom)
estimate_next_facing(F, forward, F).
estimate_next_facing(top, left, left).
estimate_next_facing(top, right, right).
estimate_next_facing(top, backward, bottom) :- !.
estimate_next_facing(right, left, top).
estimate_next_facing(right, right, bottom).
estimate_next_facing(right, backward, left) :- !.
estimate_next_facing(left, left, bottom).
estimate_next_facing(left, right, top).
estimate_next_facing(bottom, left, right).
estimate_next_facing(bottom, right, left).
estimate_next_facing(F, backward, B) :-
    estimate_next_facing(B, backward, F).

% estimate_next_position(+int, +int, +atom, -int, -int)
estimate_next_position(X, Y, top, X1, Y) :-
    X1 is X + 1.
estimate_next_position(X, Y, bottom, X1, Y) :-
    X1 is X - 1.
estimate_next_position(X, Y, left, X, Y1) :-
    Y1 is Y - 1.
estimate_next_position(X, Y, right, X, Y1) :-
    Y1 is Y + 1.

opposite(left, right).
opposite(right, left).

% ----------------------------------------------------------------------------------------------------------------------
% Peceptions and actions
% ----------------------------------------------------------------------------------------------------------------------

% move(+atom)
move(Direction) :-
    println(["I'm trying to move ", Direction]),
    motor <- move(Direction).

clean_up :-
    println(["I'm trying to cleaning up"]),
    sucker <- cleanUp.

reset_odometer :-
    count_steps(_).

% count_steps(?int)
count_steps(Steps) :-
    motor <- countSteps returns Steps,
    println(["I walked ", Steps, " steps since the last check"]).

% scan(?atom)
scan(Dirtness) :-
    scanner <- scan returns D,
    println(["It's ", D, " here"]),
    D = Dirtness.


% ----------------------------------------------------------------------------------------------------------------------
% Belief base management
% ----------------------------------------------------------------------------------------------------------------------

% get(?term, ?term)
get(K, V) :-
    belief(K, V).

% store(?term, ?term)
store(K, V) :-
    assertz(belief(K, V)).

% update(?term, ?term)
update(K, V) :-
    retract(belief(K, _)),
    assertz(belief(K, V)).


% ----------------------------------------------------------------------------------------------------------------------
% Logging facilities
% ----------------------------------------------------------------------------------------------------------------------

println([]) :- !, nl.
println([X | Xs]) :- write(X), !, println(Xs).

log_current_position(X, Y) :-
    println(["I believe I'm in (", X, ", ", Y, ")"]).


% ----------------------------------------------------------------------------------------------------------------------
% Misc algorithms
% ----------------------------------------------------------------------------------------------------------------------

% max(+list, ?term)
max(XS, Max) :-
    max(XS, min, Max).
max([], Max, Max).
max([X | XS], min, Max) :- !,
  max(XS, X, Max).
max([X | XS], Old, Max) :-
  Old < X, !,
  max(XS, X, Max).
max([X | XS], Old, Max) :-
  Old >= X,
  max(XS, Old, Max).

% min(+list, ?term)
min(XS, Min) :-
    min(XS, max, Min).
min([], Min, Min).
min([X | XS], max, Min) :- !,
  min(XS, X, Min).
min([X | XS], Old, Min) :-
  Old > X, !,
  min(XS, X, Min).
min([X | XS], Old, Min) :-
  Old =< X,
  min(XS, Old, Min).
